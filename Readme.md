# Get started

```php
<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

final class SomeClass
{
    /**
     * @var string
     */
    private $someClassProperty;
    
    public function __construct(string $someClassProperty)
    {
        $this->someClassProperty = $someClassProperty;
    }
    
    public function getSomeClassProperty(): string
    {
        return $this->someClassProperty;
    }
}

final class Dto
{
    /**
     * @var string
     */
    private $string;
    /**
     * @var float
     */
    private $float;
    /**
     * @var array|string[]
     */
    private $arrayOfStrings;
    /**
     * @var SomeClass
     */
    private $someClass;
    /**
     * @var array|SomeClass[]
     */
    private $arrayOfClasses;
    
    /**
     * Dto constructor.
     * @param string $string
     * @param float $float
     * @param string[] $arrayOfStrings
     * @param SomeClass $someClass
     * @param SomeClass[] $arrayOfClasses
     */
    public function __construct(
        string $string,
        float $float,
        array $arrayOfStrings,
        SomeClass $someClass,
        array $arrayOfClasses
    ) {
        
        $this->string = $string;
        $this->float = $float;
        $this->arrayOfStrings = $arrayOfStrings;
        $this->someClass = $someClass;
        $this->arrayOfClasses = $arrayOfClasses;
    }
    
    public function getString(): string
    {
        return $this->string;
    }
    
    public function getFloat(): float
    {
        return $this->float;
    }
    
    /**
     * @return string[]
     */
    public function getArrayOfStrings(): array
    {
        return $this->arrayOfStrings;
    }
    
    public function getSomeClass(): SomeClass
    {
        return $this->someClass;
    }
    
    /**
     * @return SomeClass[]
     */
    public function getArrayOfClasses(): array
    {
        return $this->arrayOfClasses;
    }
}


$dtoLib = new \ifrolikov\dto\Facade();

$dto = $dtoLib->getDtoBuilder()->setData([
    'string' => 'string',
    'float' => 1.00,
    'arrayOfString' => [
        'array of strings'
    ],
    'someClass' => [
        'someClassProperty' => 'prop1'
    ],
    'arrayOfClasses' => [
        [
            'someClassProperty' => 'prop2'
        ]
    ]
])->build(Dto::class);

print_r($dto);

$json = $dtoLib->getJsonDtoPacker()->pack($dto);
$array = $dtoLib->getArrayPacker()->pack($dto);

print_r($json);
print_r($array);
```

Result:

```
Dto Object
(
    [string:Dto:private] => string
    [float:Dto:private] => 1
    [arrayOfStrings:Dto:private] => Array
        (
        )

    [someClass:Dto:private] => SomeClass Object
        (
            [someClassProperty:SomeClass:private] => prop1
        )

    [arrayOfClasses:Dto:private] => Array
        (
            [0] => SomeClass Object
                (
                    [someClassProperty:SomeClass:private] => prop2
                )

        )

)
{"string":"string","float":1,"arrayOfStrings":[],"someClass":{"someClassProperty":"prop1"},"arrayOfClasses":[{"someClassProperty":"prop2"}]}
Array
(
    [string] => string
    [float] => 1
    [arrayOfStrings] => Array
        (
        )

    [someClass] => Array
        (
            [someClassProperty] => prop1
        )

    [arrayOfClasses] => Array
        (
            [0] => Array
                (
                    [someClassProperty] => prop2
                )

        )

)
```
